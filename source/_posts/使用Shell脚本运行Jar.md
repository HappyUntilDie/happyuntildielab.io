---
title: 使用Shell脚本运行Jar
tags:
  - Java
  - DevOps
categories:
  - Java
abbrlink: d97be0bb
date: 2021-03-22 15:04:24
updated: 2021-03-22 15:04:24
---

#### 启动脚本

```shell
#!/bin/sh
WORK_DIR=$(cd "$(dirname "$0")";pwd)
nohup java -jar ${WORK_DIR}/jenkins.war > ${WORK_DIR}/jenkins.log 2>&1
exit 0
```

#### 停止脚本

```shell
#!/bin/sh
PID=$(ps -ef | grep jenkins.war | grep -v grep | awk '{ print $2 }')
if [ -z "$PID" ]
then
echo Application is already stopped
else
echo kill $PID
kill $PID
fi
```



