---
title: Hello Hexo
tages:
  - Hexo
categories:
  - Hexo
abbrlink: a1751c09
date: 2021-01-13 10:31:09
updated: 2021-01-13 10:31:09
---
### Hexo安装和使用

安装Hexo

```bash
npm install -g hexo-cli
```

初始化站点

```
hexo init mysite
```

创建新文章

``` bash
hexo new "My New Post"
```

启动服务

``` bash
hexo server
```

<!-- MORE -->

生成静态文件

``` bash
hexo generate
```

远程发布

``` bash
hexo deploy
```

### 给文章添加图片

复制图片到source/images/*目录下，文章中使用/images/1.png方式引用

### 使用GitLab Pages发布站点

与`GitHub Pages`一样，先创建一个名字为`username`.gitlab.io的public库，`username`是自己在Gitlab的用户名。

在hexo站点文件夹下，添加`.gitlab-ci.yml`文件:

```yaml
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/hexo
image: node:10.15.3

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  cache:
    paths:
      - node_modules
    key: project
  only:
    - master
```

将站点文件推送到新创建的GitLab库，后续的新增和修改直接Commit-Push即可。

站点部署之后的访问地址为`username`.gitlab.io，更多配置查看[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)文档。

### 阅读全文

可以在文章插入`<!-- MORE -->`注释，该注释之后的部分不会在首页的文章列表中显示，会增加`阅读全文`按钮

