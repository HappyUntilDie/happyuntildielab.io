---
title: Centos 7 创建服务
abbrlink: ab68c1d3
date: 2021-03-19 18:13:25
updated: 2021-03-19 18:13:25
tags: Centos 7
---

#### 创建Service Unit文件

1、以Kafka安装教程为例，首先创建`/etc/systemd/system/zookeeper.service`文件

```
[Unit]
Requires=network.target remote-fs.target
After=network.target remote-fs.target

[Service]
Type=simple
User=kafka
ExecStart=/home/kafka/kafka/bin/zookeeper-server-start.sh /home/kafka/kafka/config/zookeeper.properties
ExecStop=/home/kafka/kafka/bin/zookeeper-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```

<!--MORE-->

2、创建Kafka服务文件`/etc/systemd/system/kafka.service`

```
[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=kafka
ExecStart=/bin/sh -c '/home/kafka/kafka/bin/kafka-server-start.sh /home/kafka/kafka/config/server.properties > /home/kafka/kafka/kafka.log 2>&1'
ExecStop=/home/kafka/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
```

#### 启动服务

```shell
sudo systemctl start kafka
```

#### 停止服务

```shell
sudo systemctl stop kafka
```

#### 查看状态

```shell
sudo systemctl status kafka
```

#### 开机启动

```shell
sudo systemctl enable kafka
```

