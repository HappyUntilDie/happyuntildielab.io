---
title: ANTLR学习--简单计算器实现
tags:
  - ANTLR
  - Java
categories:
  - Java
abbrlink: 5c37fe1
date: 2021-01-13 10:31:09
updated: 2021-01-13 10:31:09
---

#### 编写Calculator.g4文件

```ANTLR
grammar Calculator;

expr: expr op=(MUL|DIV) expr    # MulDiv
    | expr op=(ADD|SUB) expr    # AddSub
    | INT                       # int
    | '(' expr ')'              # parens
    ;

MUL: '*';
DIV: '/';
ADD: '+';
SUB: '-';

INT: [0-9]+ ;
NEWLINE: '\r'?'\n';
WS: [ \t]+ -> skip;
```

每条规则后的'# MulDIV', '# AddSub' 等方式表示给该规则标记，注解之后会生成对应的visit方法，方便使用

<!-- MORE -->

#### 根据规则生成代码

执行命令：

```shell
antlr4 Calculator.g4 -package calcuator -visitor -no-listener
```

**-package**      指定生成的包名

**-visitor**          表示生成Visitor代码

**-no-lisenter**  表示不生成Listener代码

#### *Visitor功能实现

```java
package calcuator.impl;

import calcuator.CalculatorBaseVisitor;
import calcuator.CalculatorParser;

public class CalculatorWalkVisitor extends CalculatorBaseVisitor<Integer> {

    @Override
    // 根据.g4文件中的标签生成的方法，可以对应到具体规则
    public Integer visitMulDiv(CalculatorParser.MulDivContext ctx) {
        Integer left = visit(ctx.expr(0));
        Integer right = visit(ctx.expr(1));
        // 乘或除预算
        return ctx.op.getType() == CalculatorParser.MUL ? left * right : left / right;
    }

    @Override
    public Integer visitAddSub(CalculatorParser.AddSubContext ctx) {
        // 加减运算 
        Integer left = visit(ctx.expr(0));
        Integer right = visit(ctx.expr(1));
        return ctx.op.getType() == CalculatorParser.ADD ? left + right : left - right;
    }

    @Override
    public Integer visitInt(CalculatorParser.IntContext ctx) {
        // 数字，解析返回整型 
        return Integer.valueOf(ctx.INT().getText());
    }

    @Override
    public Integer visitParens(CalculatorParser.ParensContext ctx) {
        // 匹配到括号，继续visit
        return visit(ctx.expr());
    }
}

```

#### 编写Main测试

```JAVA
package calcuator;

import calcuator.impl.CalculatorWalkVisitor;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public class Main {

    public static void main(String[] args) { 
        // 输入
        String input = "(1+2)*3+4/2";
        CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(input));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CalculatorParser parser = new CalculatorParser(tokens);
        // 新建Visitor，从expr开始执行
        final Integer integer = new CalculatorWalkVisitor().visit(parser.expr());
        System.out.println("input:\t" + input);
        System.out.println("result:\t" + integer);
    }
}

```

输出：

```shell
input:	(1+2)*3+4/2
result:	11
```

对应的语法树：

![image-20210113110333882](/images/ANTLR/image-20210113110333882.png)

#### 扩展

到此只实现了一个简单的整数计算，可以考虑加入变量以及浮点数计算



#### 学习文档

 1、 [ANTLR V4 Guide](https://pragprog.com/titles/tpantlr2/the-definitive-antlr-4-reference/)